import React from "react";
import SalesorderService from '../../data/services/salesorder-service';
import { CHAINCODE_NAME, ORG_CONFIG } from '../../constants/index'
import Acknowledgement from "./Acknowledgement";
import DataTable from 'react-data-table-component';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import TablePagination from '@mui/material/TablePagination';
//import TextField from '@material-ui/core/TextField';
import TextField from '@mui/material/TextField';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import { Grid, withWidth } from '@material-ui/core';
import EditIcon from '@mui/icons-material/Edit';
import IconButton from '@mui/material/IconButton';
import DatePicker from "react-datepicker";
//import Dropdown from 'react-dropdown';
//import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import { Form, Button, Col,Row } from 'react-bootstrap';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import Box from '@mui/material/Box';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { Redirect, useHistory, withRouter } from 'react-router-dom';


const useStyles = makeStyles((theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
      //maxWidth: "lg",
      width: 1000,
      margin: `${theme.spacing(0)} auto`
    },
    grid: {
      padding: theme.spacing(1), //grid padding
      textAlign: 'center',
    },

    loginBtn: {
      marginTop: theme.spacing(2),
      flexGrow: 1
    },
    header: {
      textAlign: 'left',
      background: '#212121',
      color: '#fff'
    },
    card: {
      marginTop: theme.spacing(10),
      width: '350px',
      boxShadow: 'rgba(255, 0, 0, 0.117647) 0px 1px 6px, rgba(255, 0, 0, 0.117647) 0px 1px 4px'

    }
  })
);


export default function SalesOrderSearch() {
  const classes = useStyles();
  React.useEffect(() => {
    //getSalesOrderTableData();
    //getFilterData();

  }, []);
  var joinVal;
  const [tableData, setTableData] = React.useState([]);
  const [jobId, setJobId] = React.useState([]);
  const [buyerPONumber, setBuyerPONumber] = React.useState([]);
  const [salesOrderNumber, setSalesOrderNumber] = React.useState([]);
  const [buyerPODate, setBuyerPODate] = React.useState([]);
  const [orderStatus, setOrderStatus] = React.useState([]);
  const [startDate, setStartDate] = React.useState([]);
  const [endDate, setEndDate] = React.useState([]);
  const [salesOrderDate, setSalesOrderDate] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [columnvisibility, setColumnVisibility] = React.useState(false);
  const [handleAction, setHandleAction] = React.useState(false);
  const history = useHistory();

  const navigateTo = (pageName) => {
    console.log('inside routing');
    history.push(pageName);
}

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handlePDFExport = () => {
    console.log('Inside handlePDFExport');

  }



  const getSalesOrderTableData = async () => {

    if(orderStatus == "Shipped" || orderStatus == "Ownership transferred")
    {
      setColumnVisibility(true);
      console.log(' selected Order status:  ',orderStatus);
      console.log('Boolean value: ',columnvisibility);
    }

    const role2 = "PM";
    const role = "Manufacturing";

    if (role == "PM") {
      setColumnVisibility(false);
    }
    else {
      setColumnVisibility(true);
    }

    console.log('Role: ', role);
    console.log('column visibility: ', columnvisibility);

    console.log("is it calling again?");
    let payload = {
      "chaincode": CHAINCODE_NAME.CHAINCODE_PLCEVENTS,
      "args": ["SearchOrderDetails", "{\"startDate\":\"" + startDate + "\",\"endDate\":\"" + endDate + "\",\"buyerPONumber\":\"" + buyerPONumber + "\",\"buyerPODate\":\"" + buyerPODate + "\",\"buyerSalesOrderNumber\":\"" + salesOrderNumber + "\",\"buyerSalesOrderNumberDate\":\"" + salesOrderDate + "\",\"orderStatus\":\"" + orderStatus + "\",\"entityId\":\"" + ORG_CONFIG.ENTITYID + "\",\"type\":\"endCustomerOrderDetail\",\"blockchainDataAccessId\":\"\"}"],
      "peer": ORG_CONFIG.PEER
    }
    console.log("input payload: " + JSON.stringify(payload));
    SalesorderService.getSalesOrderData(payload).then((res) => {
      console.log('response of sales order data', res);
      console.log('Response code: ', res.status)
      var abc;
      var arr;
     
      if (res.status == "200") {
        if (res.data && res.data.result && res.data.result.payload && res.data.result.payload.Payload) {

          if (res.data.returnCode == "Success") {
            if (res.data.result.payload.Code == "001") {
              var jobTotal = [];
              const data = res.data.result.payload.Payload;
              console.log('Final data: ', data);
              var b;
              abc = JSON.stringify(data);
              //data["entityJobId"] = "heelo";
              console.log('output data', data);
              var hh;
              for (var a = 0; a < data.length; a++) {
                //console.log("hello what you giving? " + data[a].Record.entityJobId);
                var cc = data[a].Record.entityJobId;
                for (let l = 0; l < cc.length; l++) {
                  b = cc[l].split("::")
                 // console.log("check  : " + l + ":" + b)
                  hh = b[0];
                  //console.log("split  : " + l + ":" + hh)
                  joinVal = joinVal + hh + ',';

                }

               // console.log("hiiii ->" + joinVal);

                //    d = b[0];
                //   c.push(d);
                data[a].Record["entityJobId"] = joinVal;
                joinVal = "";


              }

              console.log("JobTotal array: ", data);


              // let a,b,c,d;
              // let v = abc.Record.buyerId;
              // for(let i=0;i<v[i].length;i++)
              // {
              //    b = v[i].split("::")
              //    d = b[0];
              //   c.push(d);
              // }
              // console.log('value of final  det: ',c);


              setTableData(data);
            }
            else {
              const data = res.data.result.payload.Payload;
              alert(data);
            }

           

          }
          
        }
      }

    });

    console.log('Printing tableData: ', tableData);
    console.log('Buyer-PO-Number: ', buyerPONumber)
    console.log('Order status:', orderStatus);

  }

  const printDocument = () => {
    const input = document.getElementById('pdfdiv');
    html2canvas(input)
      .then((canvas) => {
        var imgWidth = 200;
        var pageHeight = 290;
        var imgHeight = canvas.height * imgWidth / canvas.width;
        var heightLeft = imgHeight;
        const imgData = canvas.toDataURL('image/png');
        const pdf = new jsPDF('p', 'mm', 'a4')
        var position = 0;
        var heightLeft = imgHeight;
        pdf.addImage(imgData, 'JPEG', 0, position, imgWidth, imgHeight);
        pdf.save("Smart Contract.pdf");
      });
  }

  const printJob = () => {
    alert('Inside job array.')
    navigateTo('/ack');
  }

  const getFilterData = () => {
    //await getSalesOrderTableData();
    console.log('Inside filter function.')
    //const newTableData = await getSalesOrderTableData();
    //console.log('new data ',newTableData);
    // let a,b,c,d;
    // tableData.map((r) => {
    //   let v = r.Record.entityJobId;
    //   for(let i=0;i<v[i].length;i++)
    //   {
    //      b = v[i].split("::")
    //      d = b[0];
    //     c.push(d);
    //   }
    //   console.log('value of c: ',c);
    // })
  }

  
  const columns = [
    { id: 'buyerName', label: 'Buyer Name', minWidth: 170 },
    { id: 'buyerPONumber', label: 'PO Number', minWidth: 100 },
    {id: 'buyerPODate',label: 'PO Date',minWidth: 100,},
    { id: 'productQuality', label: 'Product Quality', minWidth: 100 },
    { id: 'orderStatus', label: 'Order Status', minWidth: 100 },
    { id: 'entityJobId', label: 'Job', minWidth: 100 },
    { id: 'productQualityInspectionByEndCustomer', label: 'Quality By End Customer', minWidth: 100 },
    { id: 'acknowledgementRemarks', label: 'Acknowledgement Remark', minWidth: 100 },
    { id: 'onTimeDelivery', label: 'On time Delivery', minWidth: 100 },
    { id: 'Action', label: 'Action', minWidth: 100 }
  ];
  
  function createData(buyerName, buyerPONumber, buyerPODate, productQuality,orderStatus,entityJobId,productQualityInspectionByEndCustomer,acknowledgementRemarks,onTimeDelivery) {
    return {buyerName, buyerPONumber, buyerPODate, productQuality,orderStatus,entityJobId,productQualityInspectionByEndCustomer,acknowledgementRemarks,onTimeDelivery };
  }
  



  return (

    <div>
 <div className="content-header">
   <h3 style={{fontWeight:"bold",fontSize:'25px',marginTop:'15px'}}>Sales Order Search</h3>
    </div>
       <div className="content" style={{background:'White',marginLeft:'1%',marginRight:'1%',marginTop:'20px'}}>
         <div className="row">
           <div className="col-md-6">
           <Form>
  {/* Start Date form field */}
  <Form.Group as={Row} controlId="StartDate">
    <Form.Label column sm="4" className="bold">
Start Date
    </Form.Label>
    <Col sm="8">
    <Form.Control id="StartDate"
 type="date"   onChange={e => setStartDate(e.target.value)}
        />
    </Col>
  </Form.Group>

{/* End Date form field */}
  <Form.Group as={Row} controlId="EndDate" style={{marginTop:'10px'}}>
    <Form.Label column sm="4" className="bold">
      Buyer PO Number
    </Form.Label>
    <Col sm="8">
    <Form.Control type="date" id="EndDate" onChange={e => setEndDate(e.target.value)}
 placeholder="Enter date" />
    </Col>
  </Form.Group>

{/*  */}

  <Form.Group as={Row} controlId="formPlaintextEmail" style={{marginTop:'10px'}}>
    <Form.Label column sm="4" className="bold">
      Sales Order Status
    </Form.Label>
    <Col sm="8">
    <Form.Control type="email" />
    </Col>
  </Form.Group>

  
  <Form.Group as={Row} controlId="formPlaintextEmail" style={{marginTop:'10px'}}>
    <Form.Label column sm="4" className="bold">
    Order Status
    </Form.Label>
    <Col sm="8">
    <select className="browser-default custom-select" id="demo-simple-select"   onChange={e => setOrderStatus(e.target.value)} >
          <option>Choose your option</option>
          <option value={"PO received"}>PO received</option>
          <option value={"Currently in manufacturing"}>Currently in manufacturing</option>
          <option value={"Shipped"}>Shipped</option>
          <option value={"Ownership transferred"}>Ownership transferred</option>
          <option value={"Acknowleged receipt"}>Acknowleged receipt</option>
          <option value={"End customer invoice received"}>End customer invoice received</option>
        </select>
    </Col>
  </Form.Group>
</Form>
           </div>
           <div className="col-md-6">
           <Form.Group as={Row} controlId="formPlaintextEmail" >
    <Form.Label column sm="4" className="bold">
    End Date
    </Form.Label>
    <Col sm="8">
    <Form.Control type="date"/>
    </Col>
  </Form.Group>
  <Form.Group as={Row} controlId="formPlaintextEmail" style={{marginTop:'10px'}}>
    <Form.Label column sm="4" className="bold">
    Buyer PO Date
    </Form.Label>
    <Col sm="8">
    <Form.Control type="date" />
    </Col>
  </Form.Group>
  <Form.Group as={Row} controlId="formPlaintextEmail" style={{marginTop:'10px'}}>
    <Form.Label column sm="4" className="bold">
    Sales Order Date
    </Form.Label>
    <Col sm="8">
    <Form.Control type="date" />
    </Col>
  </Form.Group>
           </div>
           <div style={{marginLeft:"40%",marginTop:"40px"}}>
           <Button variant="primary" style={{background:"#702F8A"}}  size="md" onClick={getSalesOrderTableData} ><i class="fa fa-search"></i>&nbsp;&nbsp;<span class="bold">Search</span></Button>{' '}
  <Button variant="primary" style={{marginLeft:"25px",background:"#702F8A"}}  size="md" onClick={printDocument}><i class="fa fa-file"></i>&nbsp;&nbsp;<span class="bold">Export To PDF</span></Button>{' '} 
           </div>
         </div>
       

         </div> 
      {/* <div style={{marginTop:"40px"}}>
    <DataTable columns={columns} data={data} pagination />
      </div> */}
        <TableContainer id="pdfdiv" component={Paper} style={{marginTop:"40px"}}>
        <Table sx={{ minWidth: 750 }} size="small" aria-label="a dense table" 
        >
          <TableHead>
            <TableRow>
              <TableCell  >Buyer Name</TableCell>
              <TableCell >PO Number</TableCell> 
              <TableCell >PO Date</TableCell>
              <TableCell >Product Quality</TableCell>
              <TableCell >Order Status</TableCell>
              <TableCell >Job</TableCell>
              <TableCell >Quality By End Customer</TableCell>
              <TableCell >Acknowledgement Remark</TableCell>
              <TableCell >On time Delivery</TableCell>
              <TableCell>Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody style={{ textAlign: "center" }}>

            {tableData
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => (
                <TableRow
                  key={row.buyerId}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >

                  <TableCell>{row.Record.buyerName}</TableCell>
                  <TableCell>{row.Record.buyerPONumber}</TableCell>
                  <TableCell>{row.Record.buyerPODate}</TableCell>
                  {/* <TableCell>{row.Record.productQuality}</TableCell> */}
                  <TableCell>{row.Record.orderStatus}</TableCell>
                  <TableCell><a href="" onClick={()=>navigateTo('/job-details')}>{row.Record.entityJobId}</a></TableCell>
                  <TableCell>{row.Record.productQualityInspectionByEndCustomer}</TableCell>
                  <TableCell>{row.Record.acknowledgementRemarks}</TableCell>
                  <TableCell>{row.Record.onTimeDelivery}</TableCell>
                  <TableCell><div>
                    <IconButton aria-label="edit" onClick={()=> {history.push('/ack', { id:row.Record.buyerPONumber });}}>
                      <EditIcon />
                    </IconButton>
                  </div>
                  </TableCell>

                </TableRow>
              ))}



          </TableBody>
        </Table>

        <TablePagination
          rowsPerPageOptions={[5, 10, 20, 30]}
          component="div"
          count={tableData.length}
          page={page}
          onPageChange={handleChangePage}
          rowsPerPage={rowsPerPage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />

      </TableContainer>
 
    </div>
  );
              }

